use clap::{Args, Parser, Subcommand, ValueEnum};
use std::process;
use strum::Display;
use tokio::io::{AsyncReadExt, AsyncWriteExt};

#[derive(Parser)]
#[command(version, about)]
struct Cli {
    #[command(subcommand)]
    command: Commands,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, ValueEnum)]
enum Earbud {
    Left,
    Right,
}

#[derive(Args, Clone, Debug)]
#[group(requires_all = ["earbud", "shortcut", "action"])]
struct ShortcutsArgs {
    #[arg(required = false)]
    earbud: Earbud,
    #[arg(required = false)]
    shortcut: Shortcut,
    #[arg(required = false)]
    action: ShortcutAction,
}

#[derive(Subcommand)]
enum Commands {
    /// Device information
    Info,
    /// View and set pinch gestures
    Shortcuts {
        #[command(flatten)]
        set: Option<ShortcutsArgs>,
    },
    /// Control active noise cancellation
    Anc { mode: Option<AncMode> },
    /// Start or stop the ear tip fit test
    EarTipFitTest,
    /// Turn in-ear detection on or off
    InEarDetection { state: OnOff },
    /// Turn low latency mode on or off
    LowLatencyMode { state: OnOff },
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, ValueEnum)]
enum OnOff {
    On,
    Off,
}

#[derive(Display, Debug, Clone, Copy, PartialEq, Eq, ValueEnum)]
#[strum(serialize_all = "kebab-case")]
enum AncMode {
    High,
    Mid,
    Low,
    Adaptive,
    Transparency,
    Off,
}

impl From<AncMode> for u8 {
    fn from(val: AncMode) -> Self {
        match val {
            AncMode::Adaptive => 4,
            AncMode::High => 1,
            AncMode::Mid => 2,
            AncMode::Low => 3,
            AncMode::Transparency => 7,
            AncMode::Off => 5,
        }
    }
}

impl TryFrom<u8> for AncMode {
    type Error = bluer::Error;
    fn try_from(value: u8) -> Result<Self, Self::Error> {
        match value {
            4 => Ok(AncMode::Adaptive),
            1 => Ok(AncMode::High),
            2 => Ok(AncMode::Mid),
            3 => Ok(AncMode::Low),
            7 => Ok(AncMode::Transparency),
            5 => Ok(AncMode::Off),
            _ => Err(bluer::Error {
                kind: bluer::ErrorKind::Failed,
                message: "could not decode response from device".to_string(),
            }),
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
struct NoiseControlShortcut {
    transparency: bool,
    noise_cancellation: bool,
    off: bool,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, ValueEnum)]
enum Shortcut {
    DoublePinch,
    TriplePinch,
    PinchHold,
    DoublePinchHold,
}

#[derive(Display, Debug, Clone, Copy, PartialEq, Eq, ValueEnum)]
#[strum(serialize_all = "kebab-case")]
enum ShortcutAction {
    SkipForward,
    SkipBack,
    VoiceAssistant,
    VolumeUp,
    VolumeDown,
    NoiseControlTransparencyOff,
    NoiseControlTransparencyActive,
    NoiseControlTransparencyActiveOff,
    NoiseControlActiveOff,
    NoAction,
}

impl From<ShortcutAction> for u8 {
    fn from(val: ShortcutAction) -> Self {
        match val {
            ShortcutAction::SkipBack => 0x08,
            ShortcutAction::SkipForward => 0x09,
            ShortcutAction::VoiceAssistant => 0x0b,
            ShortcutAction::VolumeUp => 0x12,
            ShortcutAction::VolumeDown => 0x13,
            ShortcutAction::NoiseControlActiveOff => 0x14,
            ShortcutAction::NoiseControlTransparencyActive => 0x16,
            ShortcutAction::NoiseControlTransparencyActiveOff => 0xa,
            ShortcutAction::NoiseControlTransparencyOff => 0x15,
            ShortcutAction::NoAction => 0x01,
        }
    }
}

impl TryFrom<u8> for ShortcutAction {
    type Error = bluer::Error;
    fn try_from(value: u8) -> Result<Self, Self::Error> {
        match value {
            0x08 => Ok(ShortcutAction::SkipBack),
            0x09 => Ok(ShortcutAction::SkipForward),
            0x0b => Ok(ShortcutAction::VoiceAssistant),
            0x12 => Ok(ShortcutAction::VolumeUp),
            0x13 => Ok(ShortcutAction::VolumeDown),
            0x14 => Ok(ShortcutAction::NoiseControlActiveOff),
            0x16 => Ok(ShortcutAction::NoiseControlTransparencyActive),
            0xa => Ok(ShortcutAction::NoiseControlTransparencyActiveOff),
            0x15 => Ok(ShortcutAction::NoiseControlTransparencyOff),
            0x01 => Ok(ShortcutAction::NoAction),
            _ => Err(bluer::Error {
                kind: bluer::ErrorKind::Failed,
                message: "could not decode response from device".to_string(),
            }),
        }
    }
}

#[tokio::main(flavor = "current_thread")]
async fn main() -> bluer::Result<()> {
    let args = Cli::parse();
    let Ok(session) = bluer::Session::new().await else {
        eprintln!("Could not connect to Bluetooth daemon over DBus.");
        process::exit(1);
    };
    let Ok(adapter) = session.default_adapter().await else {
        eprintln!("No Bluetooth adapter found.");
        process::exit(1);
    };
    if adapter.set_powered(true).await.is_err() {
        eprintln!("Bluetooth seems to be disabled. Please enable Bluetooth.");
        process::exit(1);
    }

    let device_addresses = adapter.device_addresses().await?;
    let Some(&ear2_address) = device_addresses
        .iter()
        .find(|&addr| matches!(addr, bluer::Address([0x2C, 0xBE, 0xEB, _, _, _])))
    else {
        eprintln!("Couldn't find any Ear (2) devices connected. Make sure you're paired with your Ear (2).");
        process::exit(1);
    };

    let Ok(mut stream) = bluer::rfcomm::Stream::connect(bluer::rfcomm::SocketAddr {
        addr: ear2_address,
        channel: 15,
    })
    .await
    else {
        eprintln!("Couldn't connect to Ear (2). Make sure you're paired.");
        process::exit(1);
    };

    match args.command {
        Commands::Anc { mode: None } => {
            stream
                .write_all(&[
                    0x55, 0x60, 0x01, 0x1e, 0xc0, 0x01, 0x00, 0x0c, 0x03, 0x98, 0x19,
                ])
                .await?;
            let mut buf = [0_u8; 16];
            stream.read_exact(&mut buf).await?;
            let anc_mode: AncMode = buf[9].try_into()?;
            println!("ANC: {}", anc_mode);
        }
        Commands::Anc { mode: Some(mode) } => {
            stream
                .write_all(&[
                    0x55,
                    0x60,
                    0x01,
                    0x0f,
                    0xf0,
                    0x03,
                    0x00,
                    0xcd,
                    0x01,
                    mode.into(),
                    0x00,
                    0xc4,
                    0x47,
                ])
                .await?;
        }
        Commands::InEarDetection { state } => {
            stream
                .write_all(match state {
                    OnOff::On => &[
                        0x55, 0x60, 0x01, 0x04, 0xf0, 0x03, 0x00, 0x26, 0x01, 0x01, 0x01, 0x73,
                        0x10,
                    ],
                    OnOff::Off => &[
                        0x55, 0x60, 0x01, 0x04, 0xf0, 0x03, 0x00, 0x25, 0x01, 0x01, 0x00, 0xb2,
                        0x94,
                    ],
                })
                .await?
        }
        Commands::LowLatencyMode { state } => {
            stream
                .write_all(match state {
                    OnOff::On => &[
                        0x55, 0x60, 0x01, 0x40, 0xf0, 0x02, 0x00, 0x27, 0x01, 0x00, 0x97, 0xf7,
                    ],
                    OnOff::Off => &[
                        0x55, 0x60, 0x01, 0x40, 0xf0, 0x02, 0x00, 0x28, 0x02, 0x00, 0xa7, 0x04,
                    ],
                })
                .await?
        }
        Commands::EarTipFitTest => {
            stream
                .write_all(&[0x55, 0x60, 0x01, 0x0a, 0xc0, 0x00, 0x00, 0x29, 0x81, 0x00])
                .await?;
            stream
                .write_all(&[
                    0x55, 0x60, 0x01, 0x14, 0xf0, 0x01, 0x00, 0x2a, 0x01, 0x43, 0x16,
                ])
                .await?;
            let mut buf = [0_u8; 17];
            stream.read_exact(&mut buf).await?;
            let mut buf = [0_u8; 11];
            stream.read_exact(&mut buf).await?;
            let mut buf = [0_u8; 10];
            stream.read_exact(&mut buf).await?;
            if !buf.starts_with(&[0x55, 0x00, 0x01, 0x0d, 0xe0, 0x02]) {
                eprintln!("could not decode response from device");
                process::exit(2);
            }
            let result_to_text = |result: u8| match result {
                0 => "OK",
                _ => "adjust fit, or try different size",
            };
            println!("left ear:  {}", result_to_text(buf[8]));
            println!("right ear: {}", result_to_text(buf[9]));
        }
        Commands::Info => {
            println!("Address: {}", ear2_address);
            stream
                .write_all(&[0x55, 0x60, 0x01, 0x42, 0xc0, 0x00, 0x00, 0x03, 0xe0, 0xd1])
                .await?;
            let mut buf = [0_u8; 8];
            stream.read_exact(&mut buf).await?;
            let version_str_len: usize = buf[5].try_into().unwrap();
            let mut buf = vec![0_u8; version_str_len + 2];
            stream.read_exact(&mut buf).await?;
            let version = String::from_utf8_lossy(&buf[..version_str_len]);
            println!("Firmware version: {}", version);
            stream
                .write_all(&[0x55, 0x60, 0x01, 0x06, 0xc0, 0x00, 0x00, 0x05, 0x90, 0xdc])
                .await?;
            let mut buf = [0_u8; 64 + 64 + 18];
            stream.read_exact(&mut buf).await?;
            let serial = String::from_utf8_lossy(&buf[37..53]);
            println!("Serial number: {}", serial);
        }
        Commands::Shortcuts { set: None } => {
            stream
                .write_all(&[0x55, 0x60, 0x01, 0x18, 0xc0, 0x00, 0x00, 0x51, 0x39, 0x21])
                .await?;
            let mut buf = [0_u8; 43];
            stream.read_exact(&mut buf).await?;
            println!("left:");
            let left_double_pinch: ShortcutAction = buf[12].try_into()?;
            let left_triple_pinch: ShortcutAction = buf[20].try_into()?;
            let left_pinch_hold: ShortcutAction = buf[28].try_into()?;
            let left_double_pinch_hold: ShortcutAction = buf[36].try_into()?;
            println!("\tdouble-pinch: {}", left_double_pinch);
            println!("\ttriple-pinch: {}", left_triple_pinch);
            println!("\tpinch-hold: {}", left_pinch_hold);
            println!("\tdouble-pinch-hold: {}", left_double_pinch_hold);
            println!("right:");
            let right_double_pinch: ShortcutAction = buf[16].try_into()?;
            let right_triple_pinch: ShortcutAction = buf[24].try_into()?;
            let right_pinch_hold: ShortcutAction = buf[32].try_into()?;
            let right_double_pinch_hold: ShortcutAction = buf[40].try_into()?;
            println!("\tdouble-pinch: {}", right_double_pinch);
            println!("\ttriple-pinch: {}", right_triple_pinch);
            println!("\tpinch-hold: {}", right_pinch_hold);
            println!("\tdouble-pinch-hold: {}", right_double_pinch_hold);
        }
        Commands::Shortcuts { set: Some(set) } => {
            let command = [
                0x55_u8,
                0x60,
                0x01,
                0x03,
                0xf0,
                0x05,
                0x00,
                0x55,
                0x01,
                0x02,
                match set.earbud {
                    Earbud::Left => 0x01,
                    Earbud::Right => 0x02,
                },
                match set.shortcut {
                    Shortcut::DoublePinch => 0x02,
                    Shortcut::TriplePinch => 0x03,
                    Shortcut::PinchHold => 0x07,
                    Shortcut::DoublePinchHold => 0x09,
                },
                set.action.into(),
                0x5e,
                0xec,
            ];
            stream.write_all(&command).await?;
        }
    }

    Ok(())
}
