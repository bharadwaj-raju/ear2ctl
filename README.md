# ear2ctl

Linux controller for the Nothing Ear (2).

This was developed *only* for the Ear (2), I don't plan to support any other devices. This was tested on firmware version 1.0.1.101.

This is unofficial, not endorsed by Nothing. There is no warranty. Licensed as GPL v3.

[Technical details on how this was made](https://bharadwaj-raju.github.io/posts/nothing-ear-2-on-linux/)

## Install

If you have Rust installed, you can install it with `cargo install ear2ctl`.

Or, get the Linux x86_64 binary from the [Releases](https://gitlab.com/bharadwaj-raju/ear2ctl/-/releases) page.
